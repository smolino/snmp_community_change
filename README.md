# Ansible Role: Change community

## Description

Ansible role for installing and configuring SNMP v3 on RHEL/CentOS/Fedora/OpenSUSE/Debian/Ubuntu/ArchLinux.

# Role Variables

| Variable             | Default         | Comments (type)                                   |
| :---                 | :---            | :---                                              |
| snmp_version         | 3               | Default SNMP Version                              |
| snmp_user            | snmp            | SNMP User                                         |
| snmp_sec_level       | authPriv        | SNMP Access Mode                                  |
| snmp_auth_protocol   | SHA             | SNMP Authentication Protocol                      |
| snmp_priv_protocol   | AES             | SNMP Encryption Protocol                          |

# ansible-playbook snmpd.yml to change community to new string
# ansible-playbook snmpd_rollback.yml  to rollback to community public



## Author

* [Sergio Molino]
